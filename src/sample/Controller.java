package sample;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Controller {

    public Canvas canvas;
    public Pane pane;
    private Background background;
    private Player player;
    private Ai ai;
    private Controls controls;
    private Ball ball;

    public void initialize() {
        background = new Background(canvas);
        player = new Player(canvas);
        controls = new Controls(player);
        ball = new Ball(canvas, player);
        ai = new Ai(canvas, ball);
        ball.setAi(ai);
        pane.setOnKeyPressed(event -> controls.onKeyPressed(event));
        pane.setOnKeyReleased(event -> controls.onKeyReleased(event));
        draw();

        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
            }
        };
        animationTimer.start();
    }

    private void update() {
        draw();
        player.move();
        ai.setVelocity();
        ball.move();
        ai.move();
    }

    private void draw() {
        background.draw();
        ball.draw();
        player.draw();
        ai.draw();
        displayScore();
    }

    private void displayScore() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Arial", 24));
        gc.fillText(player.getScore() + " : " + ai.getScore(), canvas.getWidth() / 2 - 30, 50);
    }
}
