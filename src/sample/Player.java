package sample;

import javafx.scene.canvas.Canvas;

class Player extends Paddle {

    Player(Canvas canvas) {
        super(canvas);
        x = 0;
        speed = 5;
    }
}
