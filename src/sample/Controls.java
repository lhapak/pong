package sample;

import javafx.scene.input.KeyEvent;

class Controls {

    private Player player;

    Controls(Player player) {
        this.player = player;
    }

    void onKeyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case UP:
            case W:
                player.setYVelocity(-player.getSpeed());
                break;
            case DOWN:
            case S:
                player.setYVelocity(player.getSpeed());
                break;
        }
    }

    void onKeyReleased(KeyEvent event) {
        switch (event.getCode()) {
            case UP:
            case W:
            case DOWN:
            case S:
                player.setYVelocity(0);
                break;
        }
    }
}
