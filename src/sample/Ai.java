package sample;

import javafx.scene.canvas.Canvas;

class Ai extends Paddle {

    private Ball ball;

    Ai(Canvas canvas, Ball ball) {
        super(canvas);
        x = (int) canvas.getWidth() - width;
        speed = 4;
        this.ball = ball;
    }

    void setVelocity() {
        if (ball.getY() > y) {
            yVelocity = speed;
        } else if (ball.getY() < y) {
            yVelocity = -speed;
        }
    }
}
