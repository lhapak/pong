package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

class Background {

    private Canvas canvas;
    private Color color;

    Background(Canvas canvas) {
        this.canvas = canvas;
        color = Color.BLACK;
    }

    void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(color);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }
}
