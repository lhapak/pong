package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Random;

class Ball {

    private int size;
    private Color color;
    private int x;
    private int y;
    private int yVelocity;
    private int xVelocity;
    private Canvas canvas;
    private int speed;
    private Player player;
    private Ai ai;

    Ball(Canvas canvas, Player player) {
        this.canvas = canvas;
        color = Color.WHITE;
        size = 20;
        speed = 6;
        this.player = player;
        resetPosition();
    }

    int getY() {
        return y;
    }

    void setAi(Ai ai) {
        this.ai = ai;
    }

    void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(color);
        gc.fillOval(x - size / 2, y - size / 2, size, size);
    }

    void move() {
        x += xVelocity;
        y += yVelocity;

        boolean ballTouchTopEdge = y < size / 2;
        boolean ballTouchBottomEdge = y > canvas.getHeight() - size / 2;
        boolean ballTouchLeftEdge = x < size / 2;
        boolean ballTouchRightEdge = x > canvas.getWidth() - size / 2;
        boolean ballAbovePlayersBottom = y > player.getY() - player.getHeight() / 2;
        boolean ballBelowPlayersTop = y < player.getY() + player.getHeight() / 2;
        boolean ballAboveAiBottom = y > ai.getY() - ai.getHeight() / 2;
        boolean ballBelowAiTop = y < ai.getY() + ai.getHeight() / 2;

        if (ballTouchTopEdge || ballTouchBottomEdge) {
            yVelocity = -yVelocity;
        }

        if (ballTouchLeftEdge) {
            if (ballAbovePlayersBottom && ballBelowPlayersTop) {
                xVelocity = -xVelocity;
            } else {
                resetPosition();
                ai.setScore(ai.getScore() + 1);
            }
        } else if (ballTouchRightEdge) {
            if (ballAboveAiBottom && ballBelowAiTop) {
                xVelocity = -xVelocity;
            } else {
                resetPosition();
                player.setScore(player.getScore() + 1);
            }
        }
    }

    private void resetPosition() {
        x = (int) canvas.getWidth() / 2;
        y = (int) canvas.getHeight() / 2;
        setInitialVelocity();
    }

    private void setInitialVelocity() {
        Random random = new Random();
        if (random.nextInt(2) == 1) {
            xVelocity = speed;
        } else {
            xVelocity = -speed;
        }
        if (random.nextInt(2) == 1) {
            yVelocity = speed;
        } else {
            yVelocity = -speed;
        }
    }
}
