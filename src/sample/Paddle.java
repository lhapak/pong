package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

abstract class Paddle {

    int x;
    int y;
    private Canvas canvas;
    private Color color;
    int width;
    private int height;
    int yVelocity;
    int speed;
    private int score;

    Paddle(Canvas canvas) {
        this.canvas = canvas;
        color = Color.WHITE;
        width = 10;
        height = 100;
        yVelocity = 0;
        y = (int) canvas.getHeight() / 2;
        score = 0;
    }

    void setYVelocity(int yVelocity) {
        this.yVelocity = yVelocity;
    }

    int getSpeed() {
        return speed;
    }

    int getY() {
        return y;
    }

    int getHeight() {
        return height;
    }

    int getScore() {
        return score;
    }

    void setScore(int score) {
        this.score = score;
    }

    void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(color);
        gc.fillRect(x, y - height / 2, width, height);
    }

    void move() {
        if (yVelocity < 0) {
            if (y > height / 2) {
                y += yVelocity;
            }
        } else {
            if (y < canvas.getHeight() - height / 2) {
                y += yVelocity;
            }
        }
    }
}
